#include <Servo.h>
#include "header.h"

int motorLF = 6; // Motor Left Forward
int motorLB = 8; // Motor Left Backwards
int motorRF = 9; // Motor Right Forward
int motorRB = 7; // Motor Right Backwards

int sonicTrig = A0; // Ultrasonic send pin (Trig)
int sonicEcho = A1; // Ultrasonic receive pin (Echo)

int servoPin = 5;  // Pin for servo - must be PWM
Servo headServo;   // Servo handler
int servoTPD = 20; // Time Per Degree.
                   // Aproximated time per degree of rotation in ms

void setup() {
  Serial.begin(9600); // Enable serial communication

  // Initialize motor pins
  pinMode(motorLF, OUTPUT);
  pinMode(motorLB, OUTPUT);
  pinMode(motorRF, OUTPUT);
  pinMode(motorRB, OUTPUT);

  // Initialize ultrasonic sensor pins
  pinMode(sonicTrig, OUTPUT);
  pinMode(sonicEcho, INPUT);

  // Initialize servo
  headServo.attach(servoPin);
}

// Start turning each motor.
//
// positive values move the motor forward.
// Negative values move the motor backwards.
// Zero stops the motor.
void writeMotors(int left, int right) {
  Serial.print("Writemotors called with (");
  Serial.print(left);
  Serial.print(", ");
  Serial.print(right);
  Serial.println(")");
  // Left motor
  if (left < 0) {
    digitalWrite(motorLF, LOW);
    digitalWrite(motorLB, HIGH);
  } else if (left > 0) {
    digitalWrite(motorLF, HIGH);
    digitalWrite(motorLB, LOW);
  } else if (left == 0) {
    digitalWrite(motorLF, LOW);
    digitalWrite(motorLB, LOW);
  }

  // Right motor
  if (right < 0) {
    digitalWrite(motorRF, LOW);
    digitalWrite(motorRB, HIGH);
  } else if (right > 0) {
    digitalWrite(motorRF, HIGH);
    digitalWrite(motorRB, LOW);
  } else if (right == 0) {
    digitalWrite(motorRF, LOW);
    digitalWrite(motorRB, LOW);
  }
}

// Start moving in direction dir
void move(motion dir) {
  switch(dir) {
    case FORWARD: return writeMotors(1, 1);
    case REVERSE: return writeMotors(-1, -1);
    case LEFT: return writeMotors(-1, 1);
    case RIGHT: return writeMotors(1, -1);
    default: return writeMotors(0, 0);
  }
}

// Move direction dir for t milliseconds then stop.
void move(motion dir, int t) {
  move(dir);
  delay(t);
  move(STOP);
}

// Read the distance to whatever the sensor is pointing at
int readDistance() {
  // Ensure sonicTrig pin is low for at least 2 µs
  digitalWrite(sonicTrig, LOW);
  delayMicroseconds(2);

  // Send 10µs pulse
  digitalWrite(sonicTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonicTrig, LOW);

  // Read length of HIGH pulse in µs
  float timeTaken = pulseIn(sonicEcho, HIGH);
  // Calculate distance, see distance_calculation.md for details
  float distance = timeTaken * 0.01715;

  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println("cm");

  return distance;
}

// Rotate to rotation and wait until (probably) done
void blockingRotation(int rotation) {
  int oldRotation = headServo.read();
  int rotationDelta = abs(rotation - oldRotation);
  headServo.write(rotation);
  delay(rotationDelta * servoTPD);
}

// Rotate and read distance
int directionDistance(int rotation) {
  blockingRotation(rotation);
  return readDistance();
}

void loop() {
  float distance = directionDistance(180);
  if (distance < 50.0) {
    Serial.println("Move away!");
    move(REVERSE, 500);
  }
  delay(5000);


// Motor pins setup code, order should be:
// Left forwards
// Left backwards
// Right forwards
// Right backwards
// Use this code to confirm motor pin setup is correct

//    digitalWrite(motorLF, LOW);
//    digitalWrite(motorLB, LOW);
//    digitalWrite(motorRF, LOW);
//    digitalWrite(motorRB, LOW);
//
//    Serial.println("LF");
//    digitalWrite(motorLF, HIGH);
//    delay(200);
//    digitalWrite(motorLF, LOW);
//    delay(500);
//
//    Serial.println("LB");
//    digitalWrite(motorLB, HIGH);
//    delay(200);
//    digitalWrite(motorLB, LOW);
//    delay(500);
//
//    Serial.println("RF");
//    digitalWrite(motorRF, HIGH);
//    delay(200);
//    digitalWrite(motorRF, LOW);
//    delay(500);
//
//    Serial.println("RB");
//    digitalWrite(motorRB, HIGH);
//    delay(200);
//    digitalWrite(motorRB, LOW);
//    delay(500);
//
//    delay(5000);
//
}
