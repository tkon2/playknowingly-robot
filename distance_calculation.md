# Explaination of distance calculation

When we use sonic sensors, we want to know the distance to an object.
We do this by sending a pulse of sound of a given frequency, and measuring the
time that same frequency takes to bounce back. To convert that time taken into
a distance measurement we need to do some math.

In general, distance, time, and speed are related in the following ways:

```
  distance(m) = time(s) * speed(m/s)
  time(s) = distance(m) / speed(m/s)
  speed(m/s) = distance(m) / time(s)
```

*You can verify this is correct with the units, that might make it easier to*
*remember in the future too. For example `m = s * (m/s)` simplifies to `m = m`.*


What we are interested in is the function for distance. I'll repeat it here:

```
  distance(m) = time(s) * speed(m/s)
```

Before we start, we're interested in measuring centimetres, not metres, and we
know the time is measured in µs, so we can make those adjustments. Note that the
speed also has to change to match.

```
  distance(cm) = time(µs) * speed(cm/µs)
```

We know the travel time, but we've got it doubled because we're measuring the
time taken for the sound to reach the object and bounce back. We'll divide by
two to fix this. We're already measuring this in µs. Let's call our reading X
and put it into the formula.

```
  distance(cm) = (X / 2) * speed(cm/µs)
```

The speed is the speed of sound. The problem is that the speed of sound is not a
constant. It depends on air pressure and humidity. Additional sensors can be
added to adjust for these parameters, but for these robots we just have to live
with this inaccuracy. We can estimate the speed of sound at about room
temperature as 343m/s. Since our formula requires the speed to be in cm/µs,
we'll convert it before putting it in.

The unit convertions boil down to this:

```
  cm/µs = 100 (m -> cm) / 1 000 000 (s -> µs)
```

Applying the same logic we get this:

```
  speed(cm/µs) = 343m/s * (100 / 1 000 000) = 0.0343
```

Now we can put this into the original equation.

```
  distance(m) = (X / 2) * 0.0343 = x * (0.343 / 2)
```

Finally we reach the final equation.

```
  distance(cm) = x * 0.01715
```

Or, alternatively you can use this.

```
  distance(cm) = x * (1 / 28.309) = x / 58.309
```
