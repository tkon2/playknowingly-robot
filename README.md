# Playknowingly robot

Here you will find some code to get started with the robot. Very little logic is
included, I leave that to you.

The main code base is available in `robot_code/robot_code.ino`. This is the file
you'll want to open in the arduino software.

## Robot assembly

This robot was purchased from Kjell & Co. It is available
[here](https://www.kjell.com/no/produkter/elektro-og-verktoy/arduino/arduino-pakke/playknowlogy-arduino-robotbil-startpakke-p87288).

The purchase page includes documentation for putting the robot together. I
**strongly** recommend that you use this instead of the included physical guide.
It is available for viewing or download
[here](https://www.kjell.com/globalassets/mediaassets/765168_87288_manual_en_no_sv.pdf?ref=650B211AD9).

The purchase page also includes some code, but please don't download or open it,
I fear it might put you off Arduino forever if you do. Please use the code
included in this repo instead.


## Pin setup

The code includes pin definitions, but as long as the physical pins and the
pins referenced in the code match, it doesn't matter which you choose. The
exception is whether a pin is PWM or not. A PWM pin can emulate a lower voltage
by switching power on and off very quickly. This is required for some
components.

When you're setting up your robot, one way of working would be to plug
everything in wherever and adjust the code. Alternatively you can check the code
and plug in the wires in accordingly. Either way it's important to understand
that this doesn't really matter other than PWM vs non-PWM pins.

